require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(*Rails.groups)

module Banco
  class Application < Rails::Application
    config.i18n.enforce_available_locales = false
    config.i18n.available_locales = ['pt-BR']
    config.i18n.default_locale = :'pt-BR'
    config.i18n.fallbacks = false
    config.i18n.load_path += Dir["#{Rails.root}/config/locales/**/*.{rb,yml}"]
    config.time_zone = 'Brasilia'
    config.autoload_paths += %W( #{config.root}/app/services/ )
    config.active_record.raise_in_transactional_callbacks = true
  end
end
