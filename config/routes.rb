Rails.application.routes.draw do
  resources :statements, only: [:new, :show, :create]
  resources :deposits, only: [:new, :create]
  resources :withdrawals, only: [:new, :create]
  resources :transfers, only: [:new, :create]
  devise_for :accounts, controllers: { registrations: 'registrations' }
  root to: 'site#index'
end
