module SiteHelper
  def message_key(key)
    return 'success' if key == 'notice'
    return 'danger' if key == 'alert'
    key
  end
end
