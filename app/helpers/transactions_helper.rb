module TransactionsHelper
  def show_target_id?(element)
    element.class != Withdrawal &&
      !(element.class == Deposit && account_signed_in?)
  end

  def show_source_id?(element)
    element.class != Deposit
  end

  def value_autofocus?(element)
    element.class == Withdrawal || (element.class == Deposit && account_signed_in?)
  end

  def transaction_title(element)
    return t(:make_deposit) if element.class == Deposit
    return t(:make_withdrawal) if element.class == Withdrawal
    t(:make_transfer)
  end
end
