module ApplicationHelper
  def render_error_messages(element)
    return '' if element.errors.empty?

    messages = element.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    sentence = I18n.t('errors.messages.not_saved',
                      count: element.errors.count,
                      resource: element.class.model_name.human.downcase)

    html = <<-HTML
    <div class="alert alert-dismissible alert-danger">
      <strong><h4>#{sentence}</h2></strong>
      <ul>#{messages}</ul>
    </div>
    HTML

    html.html_safe
  end
end
