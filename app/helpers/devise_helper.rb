module DeviseHelper
  def devise_error_messages!
    render_error_messages(resource)
  end
end
