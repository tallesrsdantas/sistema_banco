class MakeDeposit < MakeTransaction
  def initialize(target_account_email, value)
    super(nil, target_account_email, value)
    @transaction_model = Deposit
  end
end
