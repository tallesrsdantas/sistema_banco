class RequestBankStatement
  def initialize(account, start_date, end_date)
    @account = account
    @start_date = start_date.beginning_of_day
    @end_date = end_date.end_of_day
  end

  def run
    source_transactions = Transaction.where(created_at: @start_date..@end_date, source_account: @account)
    target_transactions = Transaction.where(created_at: @start_date..@end_date, target_account: @account)
    sort_transactions_by_date(source_transactions, target_transactions)
  end

  private

  def sort_transactions_by_date(source_transactions, target_transactions)
    all_transactions = source_transactions.to_a + target_transactions.to_a
    all_transactions.sort { |a, b| b.created_at <=> a.created_at }
  end
end
