# Classe responsável por realizar transações, para comportamentos específicos,
# basta definir os métodos na subclasse

class MakeTransaction
  def initialize(source_account_email, target_account_email, value)
    @source_account = Account.find_by_email(source_account_email)
    @target_account = Account.find_by_email(target_account_email)
    @value = value.to_f
  end

  def run
    transaction = build_transaction
    if valid_user_balance?
      ActiveRecord::Base.transaction do
        update_users_balance if transaction.save
      end
    else
      transaction.validate
      transaction.errors[:base] << 'Você não possui fundos para realizar a operação.'
    end
    transaction
  end

  private

  def build_transaction
    @transaction_model.new(
      source_account: @source_account,
      target_account: @target_account,
      value: @value,
      taxes: taxes
    )
  end

  def update_users_balance
    @target_account.update(
      balance: @target_account.balance + @value
    ) unless @target_account.nil?

    @source_account.update(
      balance: @source_account.balance - @value - taxes
    ) unless @source_account.nil?
  end

  def valid_user_balance?
    return true if @value.class != Fixnum && @value.class != Float
    @source_account.nil? || @source_account.balance >= @value + taxes
  end

  def taxes
    0
  end
end
