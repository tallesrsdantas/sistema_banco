class MakeWithdrawal < MakeTransaction
  def initialize(source_account_email, value)
    super(source_account_email, nil, value)
    @transaction_model = Withdrawal
  end
end
