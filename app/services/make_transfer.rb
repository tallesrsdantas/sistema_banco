class MakeTransfer < MakeTransaction
  def initialize(source_account_email, target_account_email, value)
    super(source_account_email, target_account_email, value)
    @transaction_model = Transfer
  end

  private

  def taxes
    tax = 0
    tax += 5 if small_tax_time?
    tax += 7 unless small_tax_time?
    tax += 10 if @value > 1000
    tax
  end

  def small_tax_time?
    !Date.today.sunday? &&
      !Date.today.saturday? &&
      Time.now.hour >= 9 &&
      Time.now.hour <= 18
  end
end
