# Controller responsável por definir as ações de todas as transações.
# É necessário definir os métodos transaction_class, transaction_params e call_service.

class TransactionsController < ApplicationController

  def new
    @transaction ||= transaction_class.new
  end

  def create
    @transaction = call_service
    if @transaction.persisted?
      redirect_to root_url, notice: t("success_#{transaction_class.to_s.downcase}")
    else
      render action: :new
    end
  end

end
