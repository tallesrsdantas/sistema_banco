class SiteController < ApplicationController
  def index
    if !account_signed_in?
      redirect_to new_account_session_path unless account_signed_in?
    else
      load_last_transactions
    end
  end

  private
  def load_last_transactions
    @transactions = RequestBankStatement.new(
      current_account,
      Date.today.beginning_of_day,
      Date.today.end_of_day
    ).run[0..4]
  end
end
