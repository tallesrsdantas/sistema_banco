class TransfersController < TransactionsController
  before_action :authenticate_account!
  private

  def transaction_class
    Transfer
  end

  def transaction_params
    params.require(:transfer).permit(:target_account_email, :value)
  end

  def call_service
    MakeTransfer.new(
      current_account.email,
      transaction_params[:target_account_email],
      transaction_params[:value]
    ).run
  end
end
