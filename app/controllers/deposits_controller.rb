class DepositsController < TransactionsController
  private

  def transaction_class
    Deposit
  end

  def transaction_params
    params.require(:deposit).permit(:target_account_email, :value)
  end

  def call_service
    MakeDeposit.new(
      account_signed_in? ? current_account.email : transaction_params[:target_account_email],
      transaction_params[:value]
    ).run
  end
end
