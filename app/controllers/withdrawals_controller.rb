class WithdrawalsController < TransactionsController
  before_action :authenticate_account!
  private

  def transaction_class
    Withdrawal
  end

  def transaction_params
    params.require(:withdrawal).permit(:value)
  end

  def call_service
    MakeWithdrawal.new(
      current_account.email,
      transaction_params[:value]
    ).run
  end
end
