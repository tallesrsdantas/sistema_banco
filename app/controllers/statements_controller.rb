class StatementsController < ApplicationController
  before_action :authenticate_account!
  def new
    @statement = Statement.new
  end

  def show
  end

  def create
    set_statement
    if @statement.valid?
      retrieve_transactions(@statement)
      render action: :show
    else
      render action: :new
    end
  end

  private

  def set_statement
    @statement = Statement.new(
      start_date: statement_params[:start_date],
      end_date: statement_params[:end_date]
    )
  end

  def retrieve_transactions(statement)
    @transactions = RequestBankStatement.new(
      current_account,
      Date.parse(statement.start_date),
      Date.parse(statement.end_date)
    ).run
  end

  def statement_params
    params.require(:statement).permit(:start_date, :end_date)
  end
end
