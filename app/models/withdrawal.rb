class Withdrawal < Transaction
  validates_presence_of :source_account
end
