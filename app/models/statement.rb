class Statement
  include ActiveModel::Model

  attr_accessor :start_date, :end_date

  validates_presence_of :start_date
  validates_presence_of :end_date
end
