class Transaction < ActiveRecord::Base
  belongs_to :source_account, class_name: 'Account', foreign_key: 'source_account_id'
  belongs_to :target_account, class_name: 'Account', foreign_key: 'target_account_id'
  attr_accessor :target_account_email, :source_account_email
  validates :value, numericality: { greater_than: 0 }
end
