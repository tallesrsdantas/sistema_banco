class Transfer < Transaction
  validates_presence_of :source_account
  validates_presence_of :target_account
end
