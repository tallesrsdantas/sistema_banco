class Account < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :validatable
  validates_presence_of :name

  def soft_delete
    update_attribute(:deleted_at, Time.current)
  end

  def active_for_authentication?
    super && !deleted_at
  end

  def inactive_message
    !deleted_at ? super : :deleted_account
  end
end
