class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.string :type
      t.integer :source_account_id
      t.integer :target_account_id
      t.float :value
      t.integer :taxes

      t.timestamps null: false
    end
  end
end
