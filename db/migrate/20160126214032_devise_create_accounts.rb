class DeviseCreateAccounts < ActiveRecord::Migration
  def change
    create_table(:accounts) do |t|
      ## Database authenticatable
      t.string :email,              null: false, default: ''
      t.string :encrypted_password, null: false, default: ''

      t.string :name, null: false, default: ''
      t.float :balance,              null: false, default: 0

      ## Rememberable
      t.datetime :remember_created_at

      t.timestamps null: false
    end

    add_index :accounts, :email,                unique: true
  end
end
