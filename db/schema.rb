# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160127222431) do

  create_table "accounts", force: :cascade do |t|
    t.string   "email",               default: "",  null: false
    t.string   "encrypted_password",  default: "",  null: false
    t.string   "name",                default: "",  null: false
    t.float    "balance",             default: 0.0, null: false
    t.datetime "remember_created_at"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.datetime "deleted_at"
  end

  add_index "accounts", ["email"], name: "index_accounts_on_email", unique: true

  create_table "transactions", force: :cascade do |t|
    t.string   "type"
    t.integer  "source_account_id"
    t.integer  "target_account_id"
    t.float    "value"
    t.integer  "taxes"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

end
