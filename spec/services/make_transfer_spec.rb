require 'spec_helper'
require './app/services/make_transfer'

describe MakeTransfer do
  let(:source_account) { FactoryGirl.create :source_account }
  let(:target_account) { FactoryGirl.create :target_account }
  describe '#run' do
    context 'when data is valid' do
      subject(:transfer) { MakeTransfer.new(source_account.email, target_account.email, 10).run }
      it 'should have the correct values' do
        expect(transfer.value).to be(10.0)
        expect(transfer.source_account.id).to be(source_account.id)
        expect(transfer.target_account.id).to be(target_account.id)
        expect(transfer.valid?).to be(true)
      end
    end

    context 'when source account id is nil' do
      subject(:transfer) { MakeTransfer.new(nil, target_account.email, 10).run }
      it 'should have the correct values' do
        expect(transfer.valid?).to be(false)
      end
    end

    context 'when target account id is nil' do
      subject(:transfer) { MakeTransfer.new(source_account.email, nil, 10).run }
      it 'should have the correct values' do
        expect(transfer.valid?).to be(false)
      end
    end

    context 'when value is nil' do
      subject(:transfer) { MakeTransfer.new(source_account.email, target_account.email, nil).run }
      it 'should have the correct values' do
        expect(transfer.valid?).to be(false)
      end
    end

    context 'when value is negative' do
      subject(:transfer) { MakeTransfer.new(source_account.email, target_account.email, -10).run }
      it 'should have the correct values' do
        expect(transfer.valid?).to be(false)
      end
    end

    context 'when source account id is invalid' do
      subject(:transfer) { MakeTransfer.new(nil, target_account.email, 10).run }
      it 'should have the correct values' do
        expect(transfer.valid?).to be(false)
      end
    end

    context 'when target account id is invalid' do
      subject(:transfer) { MakeTransfer.new(source_account.email, nil, 10).run }
      it 'should have the correct values' do
        expect(transfer.valid?).to be(false)
      end
    end

    context 'when value is invalid' do
      subject(:transfer) { MakeTransfer.new(source_account.email, target_account.email, 'bad value').run }
      it 'should have the correct values' do
        expect(transfer.valid?).to be(false)
      end
    end
  end

  describe '#taxes' do
    context 'when time is inside business hours and value is less than 1000' do
      subject(:taxes) { MakeTransfer.new(source_account.email, target_account.email, 10).send(:taxes) }
      it 'should have the correct value' do
        Timecop.travel(Time.new(2016, 1, 1, 9, 0, 0))
        expect(taxes).to be(5)
      end
    end
    context 'when time is inside business hours and value is bigger than 1000' do
      subject(:taxes) { MakeTransfer.new(source_account.email, target_account.email, 10_000).send(:taxes) }
      it 'should have the correct value' do
        Timecop.travel(Time.new(2016, 1, 1, 18, 0, 0))
        expect(taxes).to be(15)
      end
    end
    context 'when time is not inside business hours on weekend and value is less than 1000' do
      subject(:taxes) { MakeTransfer.new(source_account.email, target_account.email, 10).send(:taxes) }
      it 'should have the correct value' do
        Timecop.travel(Time.new(2016, 1, 2, 9, 0, 0))
        expect(taxes).to be(7)
      end
    end
    context 'when time is not inside business hours on weekend and value is bigger than 1000' do
      subject(:taxes) { MakeTransfer.new(source_account.email, target_account.email, 10_000).send(:taxes) }
      it 'should have the correct value' do
        Timecop.travel(Time.new(2016, 1, 2, 18, 0, 0))
        expect(taxes).to be(17)
      end
    end
    context 'when time is out of business hours and value is less than 1000' do
      subject(:taxes) { MakeTransfer.new(source_account.email, target_account.email, 10).send(:taxes) }
      it 'should have the correct value' do
        Timecop.travel(Time.new(2016, 1, 1, 23, 0, 0))
        expect(taxes).to be(7)
      end
    end
    context 'when time is out of business hours and value is bigger than 1000' do
      subject(:taxes) { MakeTransfer.new(source_account.email, target_account.email, 10_000).send(:taxes) }
      it 'should have the correct value' do
        Timecop.travel(Time.new(2016, 1, 1, 3, 0, 0))
        expect(taxes).to be(17)
      end
    end
  end
end
