require 'spec_helper'
require './app/services/make_withdrawal'

describe MakeWithdrawal do
  let(:source_account) { FactoryGirl.create :source_account }
  describe '#run' do
    context 'when data is valid' do
      subject(:withdrawal) { MakeWithdrawal.new(source_account.email, 10).run }
      it 'should have the correct values' do
        expect(withdrawal.value).to be(10.0)
        expect(withdrawal.source_account.id).to be(source_account.id)
        expect(withdrawal.target_account).to be(nil)
        expect(withdrawal.valid?).to be(true)
      end
    end

    context 'when account id is nil' do
      subject(:withdrawal) { MakeWithdrawal.new(nil, 10).run }
      it 'should have the correct values' do
        expect(withdrawal.valid?).to be(false)
      end
    end

    context 'when value is nil' do
      subject(:withdrawal) { MakeWithdrawal.new(source_account.email, nil).run }
      it 'should have the correct values' do
        expect(withdrawal.valid?).to be(false)
      end
    end

    context 'when value is negative' do
      subject(:withdrawal) { MakeWithdrawal.new(source_account.email, -10).run }
      it 'should have the correct values' do
        expect(withdrawal.valid?).to be(false)
      end
    end

    context 'when account id is invalid' do
      subject(:withdrawal) { MakeWithdrawal.new(-1, 10).run }
      it 'should have the correct values' do
        expect(withdrawal.valid?).to be(false)
      end
    end

    context 'when value is invalid' do
      subject(:withdrawal) { MakeWithdrawal.new(source_account.email, 'bad value').run }
      it 'should have the correct values' do
        expect(withdrawal.valid?).to be(false)
      end
    end
  end
  describe '#taxes' do
    subject(:taxes) { MakeWithdrawal.new(source_account.email, 10).send(:taxes) }
    it 'should have the correct value' do
      expect(taxes).to be(0)
    end
  end
end
