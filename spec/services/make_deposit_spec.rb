require 'spec_helper'
require './app/services/make_deposit'

describe MakeDeposit do
  describe '#run' do
    let(:target_account) { FactoryGirl.create :target_account }

    context 'when data is valid' do
      subject(:deposit) { MakeDeposit.new(target_account.email, 10).run }
      it 'should have the correct values' do
        expect(deposit.value).to be(10.0)
        expect(deposit.source_account).to be(nil)
        expect(deposit.target_account.id).to be(target_account.id)
        expect(deposit.valid?).to be(true)
      end
    end

    context 'when account id is nil' do
      subject(:deposit) { MakeDeposit.new(nil, 10).run }
      it 'should have the correct values' do
        expect(deposit.valid?).to be(false)
      end
    end

    context 'when value is nil' do
      subject(:deposit) { MakeDeposit.new(target_account.email, nil).run }
      it 'should have the correct values' do
        expect(deposit.valid?).to be(false)
      end
    end

    context 'when value is negative' do
      subject(:deposit) { MakeDeposit.new(target_account.email, -10).run }
      it 'should have the correct values' do
        expect(deposit.valid?).to be(false)
      end
    end

    context 'when account id is invalid' do
      subject(:deposit) { MakeDeposit.new(-1, 10).run }
      it 'should have the correct values' do
        expect(deposit.valid?).to be(false)
      end
    end

    context 'when value is invalid' do
      subject(:deposit) { MakeDeposit.new(target_account.email, 'bad value').run }
      it 'should have the correct values' do
        expect(deposit.valid?).to be(false)
      end
    end
  end

  describe '#taxes' do
    let(:target_account) { FactoryGirl.create :target_account }
    subject(:taxes) { MakeDeposit.new(target_account.id, 10).send(:taxes) }
    it 'should have the correct value' do
      expect(taxes).to be(0)
    end
  end
end
