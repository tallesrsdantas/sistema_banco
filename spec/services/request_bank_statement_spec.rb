require 'spec_helper'
require './app/services/request_bank_statement'

describe RequestBankStatement do
  let!(:account) { FactoryGirl.create :account }
  let!(:target_account) { FactoryGirl.create :target_account }
  let!(:withdrawal) { FactoryGirl.create(:withdrawal, source_account: account) }
  let!(:deposit) { FactoryGirl.create(:deposit, target_account: account) }
  let!(:transfer) { FactoryGirl.create(:transfer, source_account: account, target_account: target_account) }
  describe '#run' do
    context 'when data is valid for account' do
      subject(:transactions) { RequestBankStatement.new(account, Date.today, Date.today).run }
      it 'should have the correct values' do
        expect(transactions.size).to be(3)
        expect(transactions[0]).to eq(transfer)
        expect(transactions[1]).to eq(deposit)
        expect(transactions[2]).to eq(withdrawal)
      end
    end

    context 'when data is valid for target_account' do
      subject(:transactions) { RequestBankStatement.new(target_account, Date.today, Date.today).run }
      it 'should have the correct values' do
        expect(transactions.size).to be(1)
      end
    end

    context 'when data is valid but range does not have transactions' do
      subject(:transactions) { RequestBankStatement.new(account, Date.yesterday, Date.yesterday).run }
      it 'should have the correct values' do
        expect(transactions.size).to be(0)
      end
    end

    context 'when start date is greater than end date' do
      subject(:transactions) { RequestBankStatement.new(account, Date.today, Date.yesterday).run }
      it 'should have the correct values' do
        expect(transactions.size).to be(0)
      end
    end
  end
end
