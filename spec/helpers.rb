module Helpers
  def sign_in_with(email, password)
    visit new_account_session_path
    fill_in 'Email', with: email
    fill_in 'Senha', with: password
    click_button 'Entrar'
  end

  def login_user
    @request.env["devise.mapping"] = Devise.mappings[:account]
    account = FactoryGirl.create(:account)
    sign_in account
  end
end
