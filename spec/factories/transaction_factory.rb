FactoryGirl.define do
  factory :withdrawal do
    source_account
    value 10
    taxes 0
  end
  factory :deposit do
    target_account
    value 10
    taxes 0
  end
  factory :transfer do
    source_account
    target_account
    value 10
    taxes 0
  end
end
