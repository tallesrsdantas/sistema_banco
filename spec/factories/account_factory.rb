FactoryGirl.define do
  factory :account do
    email 'test@gmail.com'
    password '123456ta'
    password_confirmation '123456ta'
    name 'Test Name'
    balance 10

    factory :source_account do
      email 'source_test@gmail.com'
      name 'Source Name'
    end

    factory :target_account do
      email 'target_test@gmail.com'
      name 'Target Name'
    end
  end
end
