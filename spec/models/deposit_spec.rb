require 'rails_helper'

RSpec.describe Deposit, type: :model do
  let(:target_account) { FactoryGirl.create(:target_account) }
  let(:deposit) { Deposit.new(target_account: target_account, value: 5) }
  subject { deposit }
  it { should validate_presence_of :target_account }
  it { should validate_numericality_of(:value).is_greater_than(0) }
end
