require 'rails_helper'

RSpec.describe Withdrawal, type: :model do
  let(:source_account) { FactoryGirl.create(:source_account) }
  let(:withdrawal) { Withdrawal.new(source_account: source_account, value: 5) }
  subject { withdrawal }
  it { should validate_presence_of :source_account }
  it { should validate_numericality_of(:value).is_greater_than(0) }
end
