require 'rails_helper'

RSpec.describe Withdrawal, type: :model do
  let(:statement) { Statement.new(start_date: Date.today, end_date: Date.today) }
  subject { statement }
  it { should validate_presence_of :start_date }
  it { should validate_presence_of :end_date }
end
