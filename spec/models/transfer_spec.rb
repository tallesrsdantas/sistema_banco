require 'rails_helper'

RSpec.describe Transfer, type: :model do
  let(:source_account) { FactoryGirl.create(:source_account) }
  let(:target_account) { FactoryGirl.create(:target_account) }
  let(:transfer) { Transfer.new(source_account: source_account, target_account: target_account, value: 5) }
  subject { transfer }
  it { should validate_presence_of :source_account }
  it { should validate_presence_of :target_account }
  it { should validate_numericality_of(:value).is_greater_than(0) }
end
