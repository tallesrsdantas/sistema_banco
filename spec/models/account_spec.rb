require 'rails_helper'

RSpec.describe Account, type: :model do
  let(:account) { Account.new(email: 'test@gmail.com', password: '123456ta', password_confirmation: '123456ta', name: 'Test Name') }
  subject { account }
  it { should validate_presence_of :name }
  it { should validate_presence_of :password }
  it { should validate_presence_of :email }
end
