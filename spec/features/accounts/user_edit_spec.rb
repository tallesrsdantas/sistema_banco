require 'spec_helper'

feature 'User edits account' do
  before(:each) do
    FactoryGirl.create(:account)
    sign_in_with 'test@gmail.com', '123456ta'
  end

  scenario 'with valid fields' do
    edit_account_with 'test32@gmail.com', 'new name'
    expect(page).to have_content('Sua conta foi atualizada com sucesso.')
    expect(page).to have_content('new name')
    expect(page).to have_content('test32@gmail.com')
  end

  scenario 'with blank email' do
    edit_account_with '', 'new name'
    expect(page).to have_content('Email não pode ficar em branco')
  end

  scenario 'with invalid email' do
    edit_account_with 'test', 'new name'
    expect(page).to have_content('Email não é válido')
  end

  scenario 'with blank name' do
    edit_account_with 'test@gmail.com', ''
    expect(page).to have_content('Nome não pode ficar em branco')
  end

  def edit_account_with(email, name)
    visit edit_account_registration_path
    fill_in 'Email', with: email
    fill_in 'Nome', with: name
    click_button 'Editar Conta'
  end
end
