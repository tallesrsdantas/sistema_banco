require 'spec_helper'

feature 'User signs up' do
  scenario 'with valid fields' do
    sign_up_with 'Test Name', 'valid@example.com', 'password', 'password'
    expect(page).to have_content('Login efetuado com sucesso.')
  end

  scenario 'with invalid email' do
    sign_up_with 'Test Name', 'invalid_email', 'password', 'password'
    expect(page).to have_content('Email não é válido')
  end

  scenario 'with blank password' do
    sign_up_with 'Test Name', 'valid@example.com', '', ''
    expect(page).to have_content('Senha não pode ficar em branco')
  end

  scenario 'with blank name' do
    sign_up_with '', 'valid@example.com', 'password', 'password'
    expect(page).to have_content('Nome não pode ficar em branco')
  end

  scenario 'with wrong password confirmation' do
    sign_up_with 'Test Name', 'valid@example.com', 'password', 'password2'
    expect(page).to have_content('Confirmação de senha não é igual a Senha')
  end

  def sign_up_with(name, email, password, password_confirmation)
    visit new_account_registration_path
    fill_in 'Nome', with: name
    fill_in 'Email', with: email
    fill_in 'Senha', with: password
    fill_in 'Confirmação de senha', with: password_confirmation
    click_button 'Criar Conta'
  end
end
