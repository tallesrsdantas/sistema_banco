require 'spec_helper'

feature 'User signs up' do
  before(:each) do
    FactoryGirl.create(:account)
  end

  scenario 'with valid fields' do
    sign_in_with 'test@gmail.com', '123456ta'
    expect(page).to have_content('Login efetuado com sucesso!')
  end

  scenario 'with invalid password' do
    sign_in_with 'test@gmail.com', 'wrong password'
    expect(page).to have_content('E-mail ou senha inválidos.')
  end

  scenario 'with blank password' do
    sign_in_with 'test@gmail.com', ''
    expect(page).to have_content('E-mail ou senha inválidos.')
  end

  scenario 'with blank email' do
    sign_in_with '', '123456ta'
    expect(page).to have_content('E-mail ou senha inválidos.')
  end
end
