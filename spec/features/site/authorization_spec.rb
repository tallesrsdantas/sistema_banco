require 'spec_helper'

feature 'User opens page to' do
  before(:each) do
    FactoryGirl.create(:account)
  end

  scenario 'make deposit without logging in' do
    visit new_deposit_path
    expect(page).not_to have_content('Para continuar, efetue login ou registre-se.')
  end

  scenario 'make transfer without logging in' do
    visit new_transfer_path
    expect(page).to have_content('Para continuar, efetue login ou registre-se.')
  end

  scenario 'make withdrawal without logging in' do
    visit new_withdrawal_path
    expect(page).to have_content('Para continuar, efetue login ou registre-se.')
  end

  scenario 'request statement without logging in' do
    visit new_statement_path
    expect(page).to have_content('Para continuar, efetue login ou registre-se.')
  end
end
