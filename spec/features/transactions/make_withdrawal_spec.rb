require 'spec_helper'

feature 'User makes withdrawal' do
  before(:each) do
    FactoryGirl.create(:account)
    sign_in_with 'test@gmail.com', '123456ta'
  end

  scenario 'with valid fields' do
    make_withdrawal_with '5'
    expect(page).to have_content('Saque realizado com sucesso')
    expect(page).to have_content('R$ 5,00')
  end

  scenario 'with invalid value' do
    make_withdrawal_with 'abc'
    expect(page).to have_content('Valor deve ser maior que 0')
  end

  scenario 'with value greater than balance' do
    make_withdrawal_with '50'
    expect(page).to have_content('Você não possui fundos para realizar a operação.')
  end

  def make_withdrawal_with(value)
    visit new_withdrawal_path
    fill_in 'Valor', with: value
    click_button 'Realizar Saque'
  end
end
