require 'spec_helper'

feature 'User makes transfer' do
  before(:each) do
    FactoryGirl.create(:account)
    FactoryGirl.create(:target_account)
    sign_in_with 'test@gmail.com', '123456ta'
  end

  scenario 'with valid fields' do
    Timecop.travel(Time.new(2016, 1, 1, 9, 0, 0))
    make_transfer_with 'target_test@gmail.com', '5'
    expect(page).to have_content('Transferência realizada com sucesso')
    expect(page).to have_content('R$ 0,00')
    click_link 'Sair'
    sign_in_with 'target_test@gmail.com', '123456ta'
    expect(page).to have_content('R$ 15,00')
  end

  scenario 'with invalid taget email' do
    Timecop.travel(Time.new(2016, 1, 1, 9, 0, 0))
    make_transfer_with 'target_test', '5'
    expect(page).to have_content('Conta de destino não pode ficar em branco')
    visit root_path
    expect(page).to have_content('R$ 10,00')
  end

  scenario 'with blank taget email' do
    Timecop.travel(Time.new(2016, 1, 1, 9, 0, 0))
    make_transfer_with '', '5'
    expect(page).to have_content('Conta de destino não pode ficar em branco')
    visit root_path
    expect(page).to have_content('R$ 10,00')
  end

  scenario 'with blank value' do
    Timecop.travel(Time.new(2016, 1, 1, 9, 0, 0))
    make_transfer_with 'target_test@gmail.com', ''
    expect(page).to have_content('Valor deve ser maior que 0')
    visit root_path
    expect(page).to have_content('R$ 10,00')
  end

  scenario 'with invalid value' do
    Timecop.travel(Time.new(2016, 1, 1, 9, 0, 0))
    make_transfer_with 'target_test@gmail.com', '-10'
    expect(page).to have_content('Valor deve ser maior que 0')
    visit root_path
    expect(page).to have_content('R$ 10,00')
  end

  scenario 'with value greater than balance' do
    Timecop.travel(Time.new(2016, 1, 1, 9, 0, 0))
    make_transfer_with 'target_test@gmail.com', '50'
    expect(page).to have_content('Você não possui fundos para realizar a operação.')
    visit root_path
    expect(page).to have_content('R$ 10,00')
  end

  def make_transfer_with(target_email, value)
    visit new_transfer_path
    fill_in 'Email de destino', with: target_email
    fill_in 'Valor', with: value
    click_button 'Realizar Transferência'
  end
end
