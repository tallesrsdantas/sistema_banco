require 'spec_helper'

feature 'User requests statement' do

  before(:each) do
    account = FactoryGirl.create(:account)
    account_2 = FactoryGirl.create(:target_account)
    Timecop.travel(Time.new(2016, 1, 1))
    FactoryGirl.create(:deposit, target_account: account)
    FactoryGirl.create(:withdrawal, target_account: account)
    Timecop.travel(Time.new(2016, 1, 2))
    FactoryGirl.create(:deposit, target_account: account)
    FactoryGirl.create(:deposit, target_account: account)
    FactoryGirl.create(:transfer, source_account: account, target_account: account_2)
    Timecop.travel(Time.new(2016, 1, 3))
    FactoryGirl.create(:transfer, source_account: account_2, target_account: account)
    FactoryGirl.create(:deposit, target_account: account)
    sign_in_with 'test@gmail.com', '123456ta'
    Timecop.return
  end

  scenario 'for the first day' do
    request_statement_with('01/01/2016', '01/01/2016')
    page.should have_css("table tr", count: 3)
  end

  scenario 'for the second day' do
    request_statement_with('02/01/2016', '02/01/2016')
    page.should have_css("table tr", count: 4)
  end

  scenario 'for the third day' do
    request_statement_with('03/01/2016', '03/01/2016')
    page.should have_css("table tr", count: 3)
  end

  scenario 'for empty period' do
    request_statement_with('01/01/2015', '01/01/2015')
    page.should have_css("table tr", count: 1)
  end

  scenario 'for entire period' do
    request_statement_with('01/01/2015', '01/01/2018')
    page.should have_css("table tr", count: 8)
  end

  def request_statement_with(start_date, end_date)
    visit new_statement_path
    fill_in 'Data inicial', with: start_date
    fill_in 'Data final', with: end_date
    click_button 'Gerar Extrato'
  end
end
