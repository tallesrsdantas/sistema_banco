require 'spec_helper'

feature 'User makes deposit' do
  before(:each) do
    FactoryGirl.create(:account)
  end

  scenario 'with user logged in' do
    sign_in_with 'test@gmail.com', '123456ta'
    make_deposit_with '5'
    expect(page).to have_content('Depósito realizado com sucesso')
    expect(page).to have_content('R$ 15,00')
  end

  scenario 'with user not logged in' do
    make_deposit_with '5', 'test@gmail.com'
    expect(page).to have_content('Depósito realizado com sucesso')
    sign_in_with 'test@gmail.com', '123456ta'
    expect(page).to have_content('R$ 15,00')
  end

  scenario 'with invalid taget email' do
    make_deposit_with '5', 'target_test'
    expect(page).to have_content('Conta de destino não pode ficar em branco')
  end

  scenario 'with empty taget email' do
    make_deposit_with '5', ''
    expect(page).to have_content('Conta de destino não pode ficar em branco')
  end

  scenario 'with invalid value' do
    make_deposit_with '-5', 'test@gmail.com'
    expect(page).to have_content('Valor deve ser maior que 0')
  end

  scenario 'with empty value' do
    make_deposit_with '', 'test@gmail.com'
    expect(page).to have_content('Valor deve ser maior que 0')
  end

  def make_deposit_with(value, target_email = nil)
    visit new_deposit_path
    (fill_in 'Email de destino', with: target_email) unless target_email.nil?
    fill_in 'Valor', with: value
    click_button 'Realizar Depósito'
  end
end
